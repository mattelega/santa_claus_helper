from django.db import models


class SantaHelper(models.Model):
    name = models.CharField(max_length=30, null=False, blank=False, default='Santa helper')
    remote_addr = models.CharField(max_length=20, null=True)

    class Meta:
        unique_together = ('name', 'remote_addr')

    def __str__(self):
        return self.name
