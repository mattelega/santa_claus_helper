import random

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView, ListView, FormView

from core.forms import HelperForm
from core.models import SantaHelper


class HomeView(TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        ip_number = request.META['REMOTE_ADDR']
        SantaHelper.objects.filter(remote_addr=ip_number).delete()
        return super().get(request, *args, **kwargs)


class HelperCreateView(ListView, FormView):
    title = 'Add helper'
    template_name = 'helper_list.html'
    form_class = HelperForm
    success_url = reverse_lazy('core:helper_list')
    model = SantaHelper

    def post(self, request, *args, **kwargs):
        helper_name = request.POST['name']
        ip_number = request.META['REMOTE_ADDR']

        if not HelperCreateView.model.objects.filter(name=helper_name, remote_addr=ip_number).exists():
            HelperCreateView.model.objects.create(name=helper_name, remote_addr=ip_number)

        return HttpResponseRedirect(HelperCreateView.success_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ip_number'] = self.request.META['REMOTE_ADDR']
        return context


class HelperResultView(View):
    def _draw_grantees(self, helpers):
        grantees = []
        for_draw = [_ for _ in helpers]
        for name in helpers:
            grantee = random.choice(for_draw)
            while grantee == name:
                grantee = random.choice(for_draw)
            grantees.append(grantee)
            for_draw.remove(grantee)
        return grantees

    def post(self, request):
        ip_number = request.META['REMOTE_ADDR']
        santa_helpers = SantaHelper.objects.filter(remote_addr=ip_number)

        helper_names = [helper.name for helper in santa_helpers]
        grantee_names = helper_names
        if len(helper_names) > 1:
            grantee_names = self._draw_grantees(helper_names)
        helper_grantee_pairs = tuple(zip(helper_names, grantee_names))

        context = {'pairs': helper_grantee_pairs}
        return render(request, template_name='result.html', context=context)
