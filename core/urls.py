from django.urls import path

from core.views import HomeView, HelperCreateView, HelperResultView

app_name = 'core'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('SantaHelper/list/', HelperCreateView.as_view(), name='helper_list'),
    path('SantaHelper/result/', HelperResultView.as_view(), name='result'),
]
