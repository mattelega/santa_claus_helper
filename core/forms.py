from django import forms


class HelperForm(forms.Form):
    name = forms.CharField(max_length=30, required=True)
